import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;

import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputListener;

public class MainFractalPanel extends FractalPanel{
	JuliaPanel jp;

	//Mouse handling
	Point startPt;
	Point endPt;
	boolean dragging = false;
	BufferedImage orgImg;

	private Complex userSelectedPoint = new Complex(0, 0);
	
	private static final long serialVersionUID = 1L;

	public MainFractalPanel(FractalExplorer parent){
		super(parent);		
	}

	@Override
	public void init(ControlPanel c){
		throw new UnsupportedOperationException("Use other init method!");
	}

	//Custom init for MandelbrotPanel
	public void init(ControlPanel c, JuliaPanel j){
		super.init(c);		
		jp = j;		

		MPListener mpl = new MPListener(this);
		this.addMouseListener(mpl);
		this.addMouseMotionListener(mpl);
	}

	@Override
	public int getIt(Complex c){
		return getNumberOfIterations(c, c);
	}

	public Complex getSelectedPoint(){

		return userSelectedPoint;
	}

	public void zoomIn(){
		zoom(0.8);
	}

	public void zoomOut(){
		zoom(1.25);
	}

	public void zoom(double zoomAmount){ //zoomAmount is how much to show relative to before
		//Find centre
		Complex c = findCentre();

		//Take zoomAmount * range
		double newRRange = rRange * zoomAmount;
		double newIRange = iRange * zoomAmount;


		//Define new sizes
		double newRMin = c.r() - newRRange/2;
		double newRMax = c.r() + newRRange/2;
		double newIMin = c.i() - newIRange/2;
		double newIMax = c.i() + newIRange/2;

		//Update
		super.updateAxis(newRMin, newRMax, newIMin, newIMax); //r min max,  i min max
		cp.updateAxis(new Complex(newRMin, newIMax), new Complex(newRMax,newIMin)); //tl, br
		
		rRange = newRRange;
		iRange = newIRange;
	}

	public Complex findCentre(){
		double x = (rMax + rMin) /2;
		double y = (iMax + iMin) /2;

		return new Complex(x, y);
	}

	public void resetZoom(){
		super.updateAxis(-2, 2, -1.6, 1.6);

		cp.updateAxis(new Complex(-2, 1.6), new Complex(2,-1.6));
	}
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);

		Point sPt;
		Point ePt;

		//Draw rectangle
		if(dragging){

			synchronized (this) {
				sPt = startPt;
				ePt = endPt;
				img = cloneImage(orgImg); //Clear rectangle
			}

			sPt = SwingUtilities.convertPoint(parent, sPt, parent.getFPContainer());
			ePt = SwingUtilities.convertPoint(parent, ePt, parent.getFPContainer());

			Graphics gImg = img.getGraphics();
			gImg.setColor(Color.RED);

			//Work out sizes
			if(sPt.x > ePt.x && sPt.y > ePt.y){//End point is to top left of start
				gImg.drawRect(ePt.x, ePt.y, sPt.x - ePt.x, sPt.y - ePt.y); 
			} else if (sPt.x > ePt.x){ //End is bottom left
				gImg.drawRect(ePt.x, sPt.y, sPt.x - ePt.x, ePt.y - sPt.y); 
			} else if (sPt.y > ePt.y){ //End is top right
				gImg.drawRect(sPt.x, ePt.y, ePt.x - sPt.x, sPt.y - ePt.y); 
			} else { //Expected position - bottom right
				gImg.drawRect(sPt.x, sPt.y, ePt.x - sPt.x, ePt.y - sPt.y); 
			}
		}

	}

	static BufferedImage cloneImage(BufferedImage b) {
		//Get original properties
		ColorModel cm = b.getColorModel();
		boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();

		//Clone
		WritableRaster raster = b.copyData(null);
		return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}

	class MPListener implements MouseInputListener{

		MainFractalPanel m;

		public MPListener(MainFractalPanel mp) {
			m = mp;
		}

		@Override
		public void mouseClicked(MouseEvent e) {//Cursor is not moved	
			//Calculate click position relative to panel - taking into account borders of window and controls panel
			Point pt = SwingUtilities.convertPoint(parent, e.getPoint(), parent.getFPContainer());

			Complex clickLocation = pixelToComplex(pt);
			userSelectedPoint = clickLocation;

			cp.updateUserSelectedPoint(clickLocation);
			jp.setPointC(clickLocation);
		}

		@Override
		public void mouseEntered(MouseEvent e) {}

		@Override
		public void mouseExited(MouseEvent e) {}

		@Override
		public void mousePressed(MouseEvent e) {
			synchronized (this) {
				startPt = e.getPoint();
				orgImg = cloneImage(img); //Store original image
			}

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			if(dragging){

				//min and max for Y are swapped due to flipped Y axis
				//Find corner
				//Top left

				int tlX = Math.min(startPt.x, endPt.x); 
				int tlY = Math.min(startPt.y, endPt.y);

				Point tl = SwingUtilities.convertPoint(parent, tlX, tlY, parent.getFPContainer());

				//Bottom right
				int brX = Math.max(startPt.x, endPt.x);
				int brY = Math.max(startPt.y, endPt.y);

				Point br = SwingUtilities.convertPoint(parent, brX, brY, parent.getFPContainer());

				updateAxis(pixelToComplex(tl), pixelToComplex(br)); 

			}
			dragging = false;
			
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			//Check if left or right click
			if(SwingUtilities.isLeftMouseButton(e)){
				Point pt = SwingUtilities.convertPoint(parent, e.getPoint(), parent.getFPContainer());

				Complex clickLocation = pixelToComplex(pt);
				userSelectedPoint = clickLocation;

				cp.updateUserSelectedPoint(clickLocation);
				jp.setPointC(clickLocation);
			} else {
				synchronized (this) {
					endPt = e.getPoint();
				}

				dragging = true;
				repaint();
			}
			
			
		}

		@Override
		public void mouseMoved(MouseEvent e) {}

	}

}
