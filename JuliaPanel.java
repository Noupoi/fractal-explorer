public class JuliaPanel extends FractalPanel{
	Complex c = new Complex(0,0);

	private static final long serialVersionUID = 1L;

	public JuliaPanel(FractalExplorer parent){
		super(parent);		
	}

	@Override
	public int getIt(Complex d){
		return getNumberOfIterations(c, d);
	}

	public void setPointC(Complex pointC){
		c = pointC;
		cI.recompute();
		this.repaint();
	}

	public Complex getPointC(){
		return c;
	}


}