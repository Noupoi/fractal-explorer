import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.UIManager;


public class FractalExplorer extends JFrame {
	JPanel FractalPanel;

	private static final long serialVersionUID = 1L;

	public FractalExplorer(){
		super("Fractal Explorer");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
	}


	public void init(){

		JPanel m = new JPanel(new BorderLayout());
		this.setContentPane(m);

		// Set Look and Feel (the theme) to that of OS
		try {			
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			System.err.println("Could not set Look and Feel. " + e);
		}

		//Add sub panels		
		final MainFractalPanel mp = new MainFractalPanel(this); mp.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		final JuliaPanel j = new JuliaPanel(this); j.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		ControlPanel c = new ControlPanel(); 
		mp.init(c,j); j.init(c); c.init(mp,j);

		//Fractal panels are in own JFrame
		FractalPanel = new JPanel(new GridLayout(1, 2));
		FractalPanel.add(mp);
		FractalPanel.add(j);

		//Adding resize listener
		this.addComponentListener(new ComponentResizeListener(mp,j));

		m.add(c,BorderLayout.WEST);
		m.add(FractalPanel,BorderLayout.CENTER);


		this.pack();
		this.setVisible(true);		


		//TODO: find alternative to this
		try {
			Thread.sleep(50);

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		mp.redraw();
		j.redraw();



	}
	
	public JPanel getFPContainer(){
		return FractalPanel;
	}

	public static void main(String[] args) {		
		FractalExplorer f = new FractalExplorer();
		f.init();
	}

}

class ComponentResizeListener implements ComponentListener{
	MainFractalPanel mp;
	JuliaPanel jp;
	Timer timer;
	
	class RedrawListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			mp.redraw();
			jp.redraw();
		}
	}
	

	ComponentResizeListener(MainFractalPanel m, JuliaPanel j){
		mp = m;
		jp = j;
		
		timer = new Timer(1000, new RedrawListener());
		timer.setRepeats(false);
	}

	@Override
	public void componentHidden(ComponentEvent e) {}

	@Override
	public void componentMoved(ComponentEvent e) {}

	@Override
	public void componentResized(ComponentEvent e) {
		//Check for last resize event - only starts to redraw when user has finished resizing
		timer.restart();
	}

	@Override
	public void componentShown(ComponentEvent e) {}

}

