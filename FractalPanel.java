import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

import javax.swing.JPanel;


public abstract class FractalPanel extends JPanel{

	int itNum = 200;
	int pWidth = 5, pHeight = 5; //Size of panel in pixels
	//Size of axis
	double rMax = 2, rMin = -2;
	double iMax = 1.6, iMin = -1.6;

	double rRange, iRange; //Range of each axis

	double perPixelR, perPixelI; //Used for mapping pixels to a point in the complex plane

	int userClickX, userClickY; //Point user has clicked

	ControlPanel cp;


	BufferedImage img = new BufferedImage(5, 5, BufferedImage.TYPE_INT_RGB);
	ComputeImageThread cI;

	FractalExplorer parent;

	//Advanced controls
	protected boolean invertedAxis = false;
	private int selectedForumla = 0;
	protected int colouringAlgorithm = 0;

	int[][] pixelArr; //Number of iterations before divergence
	int[] colourArr; //Array of colours used for generating image

	int itFrequency[];
	double itStrength[];
	double totalItNum;

	protected int divergenceTest = 0; //0 for divergence, 1 for orbit trap
	protected int maxDistance = 0;

	private static final long serialVersionUID = 1L;

	public FractalPanel(FractalExplorer f){
		super();
		parent = f;
	}

	public void init(ControlPanel c){
		cp = c;
		this.setPreferredSize(new Dimension(310,310));		
		rRange = Math.abs(rMax) + Math.abs(rMin);
		iRange = Math.abs(iMax) + Math.abs(iMin);

		cI = new ComputeImageThread(this);
		cI.start();
	}

	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);


		synchronized (this) {
			pWidth = this.getWidth();
			pHeight = this.getHeight();
		}

		//Draw array to screen. Image is scaled to panel
		//TODO: make sure window is displayed before calculating new image
		g.drawImage(img.getScaledInstance(pWidth, pHeight, Image.SCALE_FAST), 0, 0, null);
	}


	public void updateAxis(double realMin, double realMax,
			double imagMin, double imagMax){

		synchronized (this) {
			rMax = realMax;
			rMin = realMin;
			iMax = imagMax;
			iMin = imagMin;
		}

		double tmpRRange = Math.max(rMax,rMin) - Math.min(rMax,rMin);
		double tmpIRange = Math.max(iMax,iMin) - Math.min(iMax,iMin);;

		synchronized (this) {
			rRange = tmpRRange; 
			iRange = tmpIRange;

			maxDistance = Integer.MIN_VALUE;
		}

		cI.recompute();
		this.repaint();
	}

	public void updateAxis(Complex tl, Complex br){ 
		synchronized (this) {
			rMax = br.r();
			rMin = tl.r();
			if(!invertedAxis){
				iMax = tl.i();
				iMin = br.i();
			} else {
				iMax = br.i();
				iMin = tl.i();
			}
		}


		double tmpRRange = Math.max(rMax,rMin) - Math.min(rMax,rMin);
		double tmpIRange = Math.max(iMax,iMin) - Math.min(iMax,iMin);

		synchronized (this) {
			rRange = tmpRRange; 
			iRange = tmpIRange;

			maxDistance = Integer.MIN_VALUE;
		}

		cp.updateAxis(tl, br);
		cI.recompute();
		this.repaint();
	}

	public synchronized void invertAxis(boolean invert){
		invertedAxis = invert;
		cI.recompute();
	}


	public void updateItNum(int newNum){
		synchronized (this) {
			itNum = newNum;
		}

		cI.recompute();
		this.repaint();
	}

	public Complex pixelToComplex(int x, int y){
		double tRMin;
		double tIMax;
		double tIMin;
		double tPerPixelR;
		double tPerPixelI;

		synchronized (this) {
			tRMin = rMin;
			tIMax = iMax;
			tIMin = iMin;
			tPerPixelR = perPixelR;
			tPerPixelI = perPixelI;
		}

		if(!invertedAxis){
			return new Complex(tRMin + Double.valueOf(x) * tPerPixelR, tIMax - Double.valueOf(y) * tPerPixelI);//Higher in the y axis is larger number, like an Argand diagram
		} else {
			return new Complex(tRMin + Double.valueOf(x) * tPerPixelR, tIMin + Double.valueOf(y) * tPerPixelI);//Higher in the y axis is lower number.
		}


	}

	public Complex pixelToComplex(Point p){
		return pixelToComplex(p.x, p.y);
	}

	public void setIterativeForumla(int sF){
		selectedForumla = sF;
		cI.recompute();
	}

	protected abstract int getIt(Complex c);

	public int getNumberOfIterations(Complex c, Complex d){
		boolean diverged = false;
		int itCount = 0;
		Complex i = d; // Z(i)
		Complex iNext; // Z(i+1)
		int tSelectedFormula;

		synchronized (this) {
			tSelectedFormula = selectedForumla;
		}

		//Formula selection
		if(tSelectedFormula == 1){ //Burning ship
			do {
				//Check if diverged
				if(diverged(i)){
					diverged = true;
				} else {
					iNext = (new Complex(Math.abs(i.r()), Math.abs(i.i()))).square().add(c);
					itCount ++;
					i = iNext;	
				}			

			} while (!diverged && itCount < itNum);
		} else if(tSelectedFormula == 2){ //Multibrot, d = 3
			do {
				//Check if diverged
				if(diverged(i)){
					diverged = true;
				} else {
					iNext = i.power(3).add(c);
					itCount ++;
					i = iNext;	
				}			

			} while (!diverged && itCount < itNum);
		} else if(tSelectedFormula == 3){ //Multibrot, d = 4
			do {
				//Check if diverged
				if(diverged(i)){
					diverged = true;
				} else {
					iNext = i.power(4).add(c);
					itCount ++;
					i = iNext;	
				}			

			} while (!diverged && itCount < itNum);	
		} else if(tSelectedFormula == 4){ //Multibrot, d = -2
			do {
				//Check if diverged
				if(diverged(i)){
					diverged = true;
				} else {
					iNext = i.square().inverse().add(c);
					itCount ++;
					i = iNext;	
				}			

			} while (!diverged && itCount < itNum);	
		} else if(tSelectedFormula == 5){ //Tricorn/Mandelbar
			do {
				//Check if diverged
				if(diverged(i)){
					diverged = true;
				} else {
					iNext = i.complexConjugate().square().add(c);
					itCount ++;
					i = iNext;	
				}			
				//System.out.println(i);
			} while (!diverged && itCount < itNum); /*

		} else if(tSelectedFormula == 6){ //Newton, z^3 -1
			do {
				//Check if diverged
				if(i.modulusSquared() >= 4){
					diverged = true;
				} else {
					//iNext = i.square().add(c); //TODO
					iNext = i.add(new Complex(-1/2, 0).multiply(((i.power(3)).add(new Complex(-1, 0)))).multiply(
							i.power(2).inverse())
							);
					itCount ++;
					i = iNext;	
				}			

			} while (!diverged && itCount < itNum);
		} else if(tSelectedFormula == 7){ //Newton, sin(z)
			do {
				//Check if diverged
				if(i.modulusSquared() >= 4){
					diverged = true;
				} else {
					//iNext = i.square().add(c); //TODO
					iNext = i.sub(new Complex(0, 1).inverse().multiply(
							(i.sub(i.inverse()).multiply(i.add(i.inverse()).inverse()))));
					itCount ++;
					i = iNext;	
				}			

			} while (!diverged && itCount < itNum);		*/
		} else { //Default to Mandelbrot
			do {
				//Check if diverged
				if(diverged(i)){
					diverged = true;
				} else {
					iNext = i.square().add(c);
					itCount ++;
					i = iNext;	
				}			

			} while (!diverged && itCount < itNum);
		}

		if(divergenceTest != 0){
			if(divergenceTest == 1){
				return (int) Math.round(Math.min(i.dist(new Complex(1, 1)), i.dist(new Complex(-1, -1))));
			} else if(divergenceTest == 2){
				return (int) Math.round(c.dist(new Complex(0, i.i())));
			} else {
				return (int) Math.round(i.dist(new Complex(i.r(),0)));
			} 
		} else {
			//Return based on if diverged
			if(diverged){
				return itCount;
			} else {
				return -1; //Does not diverge
			}
		}

	}

	public boolean diverged(Complex c){
		if(divergenceTest != 0){
			boolean bailOut = false;
			int maxDist = Integer.MIN_VALUE;

			if(divergenceTest == 1){ //Point based
				//Check distance
				if(c.dist(new Complex(1, 1)) >= 2 || c.dist(new Complex(-1, -1)) >= 2){
					maxDist = (int) Math.round(Math.max(c.dist(new Complex(1, 1)), c.dist(new Complex(-1, -1))));
					bailOut= true;
				} else {
					bailOut=  false;
				}

			} else if(divergenceTest == 2){ //x =0
				if(c.dist(new Complex(0, c.i())) >= 2){
					maxDist = (int) Math.round(c.dist(new Complex(0, c.i())));
					bailOut= true;
				} else {
					bailOut=  false;
				}
			} else { //y=0
				if(c.dist(new Complex(c.r(),0)) >= 2){
					maxDist = (int) Math.round(c.dist(new Complex(c.r(),0)));
					bailOut= true;
				} else {
					bailOut=  false;
				}	
			}

			if(maxDist > maxDistance){
				synchronized (this) {
					maxDistance = maxDist;
				}
			}
			return bailOut;

		} else {//Do simple divergence test
			if(c.modulusSquared() >= 4){
				return true;
			} else {
				return false;
			}
		}

	}

	public void setDivergenceTest(int dT){
		divergenceTest = dT;
		cI.recompute();
	}

	public void setColouringAlgorithm(int cA){
		colouringAlgorithm = cA;
		cI.recompute();
	}

	public BufferedImage getImage(){
		//How much each pixel represents
		synchronized (this) {
			perPixelR = rRange / pWidth;
			perPixelI = iRange / pHeight;

			pixelArr = new int[pWidth][pHeight];
		}

		//Copying into temp variables
		int tPWidth;
		int tPHeight;
		int tItNum;

		synchronized (this) {
			tPWidth = pWidth;
			tPHeight = pHeight;


		}

		//Calculate number of iterations before divergence
		for(int x = 0; x < tPWidth; x++){
			for (int y = 0; y < tPHeight; y++){
				pixelArr[x][y] = getIt(pixelToComplex(x, y));
			}
		}

		synchronized (this) {
			if(divergenceTest == 0){
				tItNum = itNum;
			} else {
				tItNum = maxDistance;
			}
		}

		//Make array of colours from pixelArr
		int count = 0;
		colourArr = new int[tPWidth * tPHeight * 3];		

		if(colouringAlgorithm != 0){//Histogram colouring
			//Count frequency of each iteration
			itFrequency = new int[tItNum+1];
			itStrength = new double[tItNum+1];
			totalItNum = 0;

			for(int x = 0; x < tPWidth; x++){
				for (int y = 0; y < tPHeight; y++){
					if(pixelArr[x][y] != -1){
						itFrequency[pixelArr[x][y]]++; //Increment count
					}
					totalItNum = totalItNum + 1f;
				}
			}

			if(colouringAlgorithm == 1){//One band
				//TODO: Check if orbit trapping
				setHistogramColours(0, tItNum, 176, 229, 251, 5, 54, 106);	 //Blue
			} else { //Five bands
				int[] boundary = new int[6];
				boundary[0] = 0;
				boundary[1] = (int) Math.round(tItNum * 0.01); //Percentage of iterations in each band
				boundary[2] = (int) Math.round(tItNum * 0.0125);
				boundary[3] = (int) Math.round(tItNum * 0.0875);
				boundary[4] = (int) Math.round(tItNum * 0.37);
				boundary[5] = tItNum;

				//Calculate colours 
				setHistogramColours(boundary[0], boundary[1], 173, 19, 13, 255, 255, 255);
				setHistogramColours(boundary[1], boundary[2], 0, 0, 0, 173, 19, 13);
				setHistogramColours(boundary[2], boundary[3], 255, 0, 0, 0, 0, 0);
				setHistogramColours(boundary[3], boundary[4], 0, 0, 0, 255, 0, 0);
				setHistogramColours(boundary[4], boundary[5], 255, 255, 255, 0, 0, 0);
			}


		} else { //If colouringAlgorithm == 0, do simple square root colouring
			//Colours used for shading iterations
			int colourLightR = 176, colourLightG = 229, colourLightB = 251;
			int colourDarkR = 5, colourDarkG = 54, colourDarkB = 106;

			//Iterating over each pixel, setting colours
			for (int y = 0; y < tPHeight; y++){
				for(int x = 0; x < tPWidth; x++){

					//If doesn't diverge
					if(pixelArr[x][y] == -1){ //Set black
						colourArr[count] = 0; //Red
						colourArr[count+1] = 0; //Green
						colourArr[count+2] = 0; //Blue
					} else { //Calculate colour -strength is weighted towards lower values
						//TODO: Check if orbit trapping
						double lightStrength = Math.sqrt(Double.valueOf(pixelArr[x][y]) / Double.valueOf(tItNum));
						double darkStrength = 1 - lightStrength;

						colourArr[count] = (int) (colourLightR * lightStrength + colourDarkR * darkStrength);
						colourArr[count+1] = (int) (colourLightG * lightStrength + colourDarkG * darkStrength);
						colourArr[count+2] = (int) (colourLightB * lightStrength + colourDarkB * darkStrength);
					}
					count = count + 3;
				}
			}
		}


		//Create image
		BufferedImage image = new BufferedImage(tPWidth, tPHeight, BufferedImage.TYPE_INT_RGB);
		WritableRaster raster = (WritableRaster) image.getRaster();

		System.gc();
		
		raster.setPixels(0,0,tPWidth,tPHeight,colourArr);
		return image;

	}

	public void setHistogramColours(int start, int end,
			double sR, double sG, double sB, //Starting colours
			double eR, double eG, double eB){ //End colours

		int tPWidth;
		int tPHeight;
		int count = 0;

		synchronized (this) {
			tPWidth = pWidth;
			tPHeight = pHeight;
		}

		//Calculate strength of each iteration
		for(int i = start; i < end; i++){
			if(i != 0){
				itStrength[i] = itStrength[i-1] + (((double) itFrequency[i]) / totalItNum);
			}
		}

		//Iterating over each pixel, setting colours
		for (int y = 0; y < tPHeight; y++){
			for(int x = 0; x < tPWidth; x++){

				//If doesn't diverge
				if(pixelArr[x][y] == -1){ //Set black
					colourArr[count] = 0; //Red
					colourArr[count+1] = 0; //Green
					colourArr[count+2] = 0; //Blue
				} else if(start<= pixelArr[x][y] && pixelArr[x][y] <= pixelArr[x][y]){ //Calculate colour

					//Strength is weighted towards lower values
					double lightStrength = itStrength[pixelArr[x][y]];
					double darkStrength = 1 - lightStrength;
					colourArr[count] = (int) (sR * lightStrength + eR * darkStrength);
					colourArr[count+1] = (int) (sG * lightStrength + eG * darkStrength);
					colourArr[count+2] = (int) (sB * lightStrength + eB * darkStrength);

				}
				count = count + 3;
			}
		}
	}

	public void setImage(BufferedImage image){
		synchronized (this) {
			img = image;
		}
		this.repaint();
	}

	public void redraw(){
		cI.recompute();
	}

	public FractalExplorer getParent(){
		return parent;
	}

}