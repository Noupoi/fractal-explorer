


public class ComputeImageThread extends Thread {
	volatile boolean draw;
	FractalPanel p;

	public ComputeImageThread(FractalPanel parent) {
		super();
		p = parent;
		draw = false;
		this.setDaemon(true);
	}

	@Override
	public void run(){
		boolean redraw; //Temp method variable

		while(true){
			//Check up to 62.5 times a second. Reduces CPU usage.
			try {
				Thread.sleep(16);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			synchronized(this){ 
				redraw = draw;
			}

			if(redraw){
				//Compute image
				p.setImage(p.getImage());
				
				//Finish				
				draw = false;
			}
		}
	}

	public synchronized void recompute(){
		draw = true;
	}

}
