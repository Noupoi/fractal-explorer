
public class Complex {

	private double r, i;
	private String name;

	public Complex(double real, double im) {
		r = real;
		i = im;
		name = null;
	}

	public Complex(double real, double im, String n) {
		r = real;
		i = im;
		name = n;
	}
		

	//Accessor methods
	public double i(){
		return this.i;
	}

	public double r(){
		return this.r;
	}	

	//Other methods
	public Complex square(){
		double re = (this.r * this.r) - (this.i * this.i);
		double im = 2 * r * i;

		return new Complex(re, im);
	}

	public double modulusSquared(){
		return  (this.r * this.r) + (this.i * this.i);
	}

	public Complex add(Complex c){
		return new Complex(r + c.r(), i + c.i());
	}
	
	public Complex sub(Complex c){
		return new Complex(r - c.r(), i - c.i());
	}
	
	public Complex multiply(Complex c){
		double re = (this.r * c.r() - this.i * c.i);
		double im = (this.r * c.i() + this.i * c.r());
		
		return new Complex(re, im);
	}
	
	public Complex inverse(){ //Find multiplicative inverse
		double re = r / (r*r + i*i);
		double im = -i / (r*r + i*i);
		
		return new Complex(re, im);
	}
	
	public Complex power(int p){
		Complex original = new Complex(r, i);
		Complex t = original;
		
		for(int i = 1; i < p; i++){ //Start at 1 as z^1 = z
			t = t.multiply(original);
		}
				
		return t;
	}

	
	public Complex complexConjugate(){
		return new Complex(r, -i);
	}
	
	public double dist(Complex c){
		return Math.sqrt((r-c.r())*(r-c.r()) + (i - c.i())*(i - c.i()) );
	}
	
	@Override
	public String toString(){
		//Check if this has a name
		if(name != null){
			if(i()>=0){
				return(name + ": " + r() + " + " + i() + "i");
			} else {
				return(name + ": " + r() + " - " + Math.abs(i()) + "i");
			}
		} else {
			//Formatting
			if(i()>=0){
				return(r() + " + " + i() + "i");
			} else {
				return(r() + " - " + Math.abs(i()) + "i");
			}
		}

	}

}
