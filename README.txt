This application was created to the specification in "specification.pdf".

Command to compile program:
javac -cp forms-1.3.0.jar *.java

Command to run program:
java -cp forms-1.3.0.jar; FractalExplorer

The ControlPanel class utilises the JGoodies FormLayout library available here: http://www.jgoodies.com/freeware/libraries/forms/

The axis of the fractal panels is laid out as if it were an Argand diagram- up and right is positive. It can be flipped with the 'Invert Y-axis checkbox'

Left mouse drag will change the Julia set preview, and right or middle drag will zoom.

Extensions completed:
Helper threads are used to compute the images
Choice of colouring schemes - square root and histograms
Added iterative formula selection - Burning ship fractal, Multibrot sets, Tricorn
Added selection of orbit traps to choose from.
Old image is scaled while new image is being computed
Provide live updates to Julia set as the mouse is dragged
Added checkbox to toggle flipping the Y-axis.
Added zoom in, out, reset buttons
User can set a name for their favourite Julia Set images.

Known issues:
Combinations of demanding settings can cause errors. (e.g. histogram colouring and orbit traps)