import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.JCheckBox;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JRadioButton;


public class ControlPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private JTextField txtRMax;
	private JTextField txtRMin;
	private JTextField txtIMin;
	private JTextField txtIMax;
	private JTextField txtItNum;
	private JTextField txtSelectedPoint;

	private MainFractalPanel mp;
	private JuliaPanel jp;

	private JComboBox<Complex> cbFav;
	private ArrayList<Complex> favPoints;

	boolean invertedAxis = false;
	
	private JCheckBox chckbxInvertYAxis;

	private JComboBox<String> cbItFormula;
	private JButton btnZoomReset;

	public ControlPanel(){
		super();

		setBorder(new EmptyBorder(8, 8, 8, 8));
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		//Creating GUI - FormLayout library from http://www.jgoodies.com/freeware/libraries/forms/
		JPanel panelStats = new JPanel();
		add(panelStats);
		panelStats.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
				new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));

		JLabel lblRendering = new JLabel("Stats");
		lblRendering.setFont(new Font("Tahoma", Font.BOLD, 11));
		panelStats.add(lblRendering, "2, 2");

		JSeparator separator = new JSeparator();
		panelStats.add(separator, "4, 2, 3, 1");

		JLabel lblSelectedPoint = new JLabel("Selected point");
		panelStats.add(lblSelectedPoint, "4, 4, right, default");

		txtSelectedPoint = new JTextField();
		txtSelectedPoint.setEditable(false);
		txtSelectedPoint.setHorizontalAlignment(SwingConstants.CENTER);
		txtSelectedPoint.setText("0 + 0i");
		panelStats.add(txtSelectedPoint, "6, 4, fill, default");
		txtSelectedPoint.setColumns(10);

		JPanel panelFavourites = new JPanel();
		add(panelFavourites);
		panelFavourites.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(100dlu;default):grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(97dlu;default)"),},
				new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));

		JLabel lblFavourites = new JLabel("Favourites");
		lblFavourites.setFont(new Font("Tahoma", Font.BOLD, 11));
		panelFavourites.add(lblFavourites, "2, 2");

		JSeparator separator_3 = new JSeparator();
		panelFavourites.add(separator_3, "4, 2, 3, 1");

		cbFav = new JComboBox<Complex>();
		panelFavourites.add(cbFav, "4, 4, 3, 1, fill, default");
		//Listener for JComboBox
		cbFav.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				//Check if null is selected
				if(cbFav.getSelectedItem() != null){
					//Update stats
					txtSelectedPoint.setText(cbFav.getSelectedItem().toString());
					jp.setPointC((Complex) cbFav.getSelectedItem());

				}
			}
		});

		JButton btnAddFav = new JButton("Add");
		panelFavourites.add(btnAddFav, "4, 6");
		btnAddFav.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String newName = JOptionPane.showInputDialog("Enter a name"); 			

				//Check if user entered a valid string
				if(newName != null && newName.length() > 0){
					favPoints.add(new Complex(mp.getSelectedPoint().r(), mp.getSelectedPoint().i(), newName));
				} else {
					favPoints.add(mp.getSelectedPoint());
				}
				resetFavComboBox();				
			}
		});

		JButton btnRemoveFav = new JButton("Remove");
		panelFavourites.add(btnRemoveFav, "6, 6");
		btnRemoveFav.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				favPoints.remove(cbFav.getSelectedItem());
				resetFavComboBox();
			}
		});
		
		JPanel panelAxis = new JPanel();
		add(panelAxis);
		panelAxis.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(10dlu;default):grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
				new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),}));

		JLabel lblAxis = new JLabel("Axis");
		lblAxis.setFont(new Font("Tahoma", Font.BOLD, 11));
		panelAxis.add(lblAxis, "2, 2");

		JSeparator separator_1 = new JSeparator();
		panelAxis.add(separator_1, "4, 2, 7, 1");

		JLabel lblReal = new JLabel("Real");
		panelAxis.add(lblReal, "4, 4, right, default");

		txtRMin = new JTextField();
		txtRMin.setHorizontalAlignment(SwingConstants.CENTER);
		txtRMin.setText("-2.0");
		panelAxis.add(txtRMin, "6, 4, fill, default");
		txtRMin.setColumns(4);

		JLabel lblTo = new JLabel("to");
		panelAxis.add(lblTo, "8, 4, right, default");

		txtRMax = new JTextField();
		txtRMax.setHorizontalAlignment(SwingConstants.CENTER);
		txtRMax.setText("+2.0");
		panelAxis.add(txtRMax, "10, 4, fill, default");
		txtRMax.setColumns(4);

		JLabel lblImaginary = new JLabel("Imaginary");
		panelAxis.add(lblImaginary, "4, 6, right, default");

		txtIMin = new JTextField();
		txtIMin.setHorizontalAlignment(SwingConstants.CENTER);
		txtIMin.setText("-1.6");
		panelAxis.add(txtIMin, "6, 6, fill, default");
		txtIMin.setColumns(4);

		JLabel lblTo_1 = new JLabel("to");
		panelAxis.add(lblTo_1, "8, 6, right, default");

		txtIMax = new JTextField();
		txtIMax.setHorizontalAlignment(SwingConstants.CENTER);
		txtIMax.setText("+1.6");
		panelAxis.add(txtIMax, "10, 6, fill, default");
		txtIMax.setColumns(4);

		JPanel panelZoomControls = new JPanel();
		panelAxis.add(panelZoomControls, "6, 8, 5, 1, center, fill");
		panelZoomControls.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
				new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));

		JButton btnZoomIn = new JButton("+");
		panelZoomControls.add(btnZoomIn, "2, 2");
		btnZoomIn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				mp.zoomIn();
			}
		});

		JButton btnZoomOut = new JButton("-");
		panelZoomControls.add(btnZoomOut, "4, 2");
		btnZoomOut.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				mp.zoomOut();
			}
		});

		btnZoomReset = new JButton("Reset");
		panelZoomControls.add(btnZoomReset, "6, 2");
		btnZoomReset.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				mp.resetZoom();	
			}
		});
		
		JPanel panelMisc = new JPanel();
		add(panelMisc);
		panelMisc.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),}));

		JLabel lblOptions = new JLabel("Misc options");
		lblOptions.setFont(new Font("Tahoma", Font.BOLD, 11));
		panelMisc.add(lblOptions, "2, 2");

		JSeparator separator_2 = new JSeparator();
		panelMisc.add(separator_2, "4, 2, 3, 1");

		JLabel lblFormula = new JLabel("Formula");
		panelMisc.add(lblFormula, "4, 4, right, default");

		cbItFormula = new JComboBox<String>();
		panelMisc.add(cbItFormula, "6, 4, fill, default");
		cbItFormula.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int index = cbItFormula.getSelectedIndex();
				mp.setIterativeForumla(index);
				jp.setIterativeForumla(index);

				System.out.println("Forumla " + index + " selected");
				
			}
		});

		JLabel lblIterations = new JLabel("Iterations");
		panelMisc.add(lblIterations, "4, 6, right, default");

		txtItNum = new JTextField();
		txtItNum.setHorizontalAlignment(SwingConstants.CENTER);
		txtItNum.setText("200");
		panelMisc.add(txtItNum, "6, 6, left, default");
		txtItNum.setColumns(4);

		JLabel lblInvertYAxis = new JLabel("Invert Y-axis");
		panelMisc.add(lblInvertYAxis, "4, 8, right, default");

		chckbxInvertYAxis = new JCheckBox("");
		panelMisc.add(chckbxInvertYAxis, "6, 8");
		chckbxInvertYAxis.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				if(chckbxInvertYAxis.isSelected()){
					invertedAxis = true;
					mp.invertAxis(true);
					jp.invertAxis(true);

				} else {
					invertedAxis = false;
					mp.invertAxis(false);
					jp.invertAxis(false);
				}
			}
		});

		JLabel lblColouringScheme = new JLabel("Colouring scheme");
		panelMisc.add(lblColouringScheme, "4, 10, right, top");

		JPanel panelColourButtons = new JPanel();
		panelMisc.add(panelColourButtons, "6, 10, fill, fill");
		panelColourButtons.setLayout(new BoxLayout(panelColourButtons, BoxLayout.Y_AXIS));


		final JRadioButton rdbtnSqrt = new JRadioButton("Simple square root");
		rdbtnSqrt.setSelected(true);
		panelColourButtons.add(rdbtnSqrt);

		final JRadioButton rdbtnSimpleHistogram = new JRadioButton("Simple histogram");
		panelColourButtons.add(rdbtnSimpleHistogram);

		final JRadioButton rdbtnFivebandedHistogram = new JRadioButton("Five-banded histogram");
		panelColourButtons.add(rdbtnFivebandedHistogram);

		//ButtonGroup for radio buttons
		ButtonGroup btnGroupColouring = new ButtonGroup();
		btnGroupColouring.add(rdbtnSqrt);
		btnGroupColouring.add(rdbtnSimpleHistogram);
		btnGroupColouring.add(rdbtnFivebandedHistogram);
		
		JLabel lblOrbitTrap = new JLabel("Orbit trap");
		panelMisc.add(lblOrbitTrap, "4, 12, right, top");
		
		JPanel panelOrbitTrapButtons = new JPanel();
		panelMisc.add(panelOrbitTrapButtons, "6, 12, fill, fill");
		panelOrbitTrapButtons.setLayout(new BoxLayout(panelOrbitTrapButtons, BoxLayout.Y_AXIS));
		
		//Radio buttons for orbit trapping
		JRadioButton rdbtnNone = new JRadioButton("None");
		rdbtnNone.setSelected(true);
		panelOrbitTrapButtons.add(rdbtnNone);
		rdbtnNone.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				mp.setDivergenceTest(0);
				jp.setDivergenceTest(0);
			}
		});
		
		JRadioButton rdbtnPoints = new JRadioButton("Points");
		panelOrbitTrapButtons.add(rdbtnPoints);
		rdbtnPoints.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				mp.setDivergenceTest(1);
				jp.setDivergenceTest(1);
			}
		});
		
		JRadioButton rdbtnLinex = new JRadioButton("Line (x=0)");
		panelOrbitTrapButtons.add(rdbtnLinex);
		rdbtnLinex.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				mp.setDivergenceTest(2);
				jp.setDivergenceTest(2);
			}
		});
		
		JRadioButton rdbtnLiney = new JRadioButton("Line (y=0)");
		panelOrbitTrapButtons.add(rdbtnLiney);
		rdbtnLiney.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				mp.setDivergenceTest(3);
				jp.setDivergenceTest(3);
			}
		});
		
		ButtonGroup btnGroupOrbitTrap = new ButtonGroup();
		btnGroupOrbitTrap.add(rdbtnNone);
		btnGroupOrbitTrap.add(rdbtnPoints);
		btnGroupOrbitTrap.add(rdbtnLinex);
		btnGroupOrbitTrap.add(rdbtnLiney);

		//Radio button listeners
		ActionListener radioButtonListener = (new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent ae) {
				if (rdbtnSqrt.isSelected()){
					mp.setColouringAlgorithm(0);
					jp.setColouringAlgorithm(0);
				} else if (rdbtnSimpleHistogram.isSelected()){
					mp.setColouringAlgorithm(1);
					jp.setColouringAlgorithm(1);
				} else if (rdbtnFivebandedHistogram.isSelected()){
					mp.setColouringAlgorithm(2);
					jp.setColouringAlgorithm(2);
				}
			}
		});

		rdbtnSqrt.addActionListener(radioButtonListener);
		rdbtnSimpleHistogram.addActionListener(radioButtonListener);
		rdbtnFivebandedHistogram.addActionListener(radioButtonListener);

		//Listeners for axis text boxes
		AxisListener aL = new AxisListener();
		txtRMax.addActionListener(aL);
		txtRMin.addActionListener(aL);
		txtIMax.addActionListener(aL);
		txtIMin.addActionListener(aL);

		//Listener for number of iterations
		txtItNum.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try{
					int newVal = Integer.parseInt(txtItNum.getText());
					if(newVal < 0 || newVal > 5000){
						throw new ArithmeticException("Number is out of range! 0 < n < 5001");
					} else {
						mp.updateItNum(newVal);
						jp.updateItNum(newVal);
					}				
				} catch (NumberFormatException n){
					System.err.println("Not updated!  " + n);
				} catch( ArithmeticException a){
					System.err.println("Not updated!  " + a);
				}

			}
		});


	}

	public void init(MainFractalPanel m, JuliaPanel j){
		//Storing reference to MandelbrotPanel		
		mp = m;
		jp = j;

		//Populate favourites
		favPoints = new ArrayList<Complex>();
		favPoints.add(new Complex(0, 0, "Default"));
		favPoints.add(new Complex(-0.8, 0.156, "A cool point"));
		favPoints.add(new Complex(0.36, -0.095, "Another cool point"));
		favPoints.add(new Complex(0.285, 0, "Third cool point"));
		favPoints.add(new Complex(0.285, 0.01, "Point V2"));
		favPoints.add(new Complex(-0.61803398874989484820458, 0, "Golden ratio"));

		resetFavComboBox();

		//Populate JComboBox
		cbItFormula.addItem("Mandelbrot set");
		cbItFormula.addItem("Burning ship");
		cbItFormula.addItem("Multibrot set (d=3)");
		cbItFormula.addItem("Multibrot set (d=4)");
		cbItFormula.addItem("Multibrot set (d=-2)");
		cbItFormula.addItem("Tricorn (Mandelbar set)");
		//cbItFormula.addItem("Newton fractal (p(z) = z^3 -1, a=1)");
		//cbItFormula.addItem("Newton fractal (p(z) = sin(z), a=1)");

	}

	public void resetFavComboBox(){
		//Adding elements in favPoints to ComboBox
		cbFav.removeAllItems();
		for(Complex c : favPoints){
			cbFav.addItem(c);
		}
	}

	public void updateUserSelectedPoint(Complex c){ //Updating selected point text box.
		txtSelectedPoint.setText(c.toString());
	}

	public void updateAxis(Complex tl, Complex br){
		txtRMin.setText(String.valueOf(tl.r()));
		txtRMax.setText(String.valueOf(br.r()));

		txtIMin.setText(String.valueOf(br.i()));
		txtIMax.setText(String.valueOf(tl.i()));
		

	}

	class AxisListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			//Update drawing
			try{
				mp.updateAxis(Double.parseDouble(txtRMin.getText()), 
						Double.parseDouble(txtRMax.getText()), 
						Double.parseDouble(txtIMin.getText()), 
						Double.parseDouble(txtIMax.getText())
						);
			} catch (NumberFormatException n){
				System.err.println("Not updated due to: " + n);
			}
		}
	}



}
